#include "motor.h"
#include <stdlib.h>
#include "cmsis_os2.h"
#include "stm32f4xx_hal.h"


static uint8_t _is_init = 0;

static TIM_HandleTypeDef htim3;
TIM_OC_InitTypeDef sConfigPWM;

void motor_PWM_init(void){

    if (!_is_init) {

        // Enable TIM3 clock //
        __HAL_RCC_TIM3_CLK_ENABLE();

        // Enable GPIOA clock 
        __HAL_RCC_GPIOA_CLK_ENABLE(); 
          
        // Initialise PB0 //

        GPIO_InitTypeDef  GPIO_InitStructure; // Define structure to initialise GPIO
        GPIO_InitStructure.Pin = GPIO_PIN_6; // Fill structure with correct parameters for pin, mode, pull and speed following lines
        GPIO_InitStructure.Mode = GPIO_MODE_AF_PP; 
        GPIO_InitStructure.Pull = GPIO_NOPULL; // No pull resistors enabled 
        GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
        GPIO_InitStructure.Alternate = GPIO_AF2_TIM3; // initialises alternate function 

        HAL_GPIO_Init(GPIOA, &GPIO_InitStructure); // Initialise GPIO using HAL function

        // Initialise timer 3 //

        htim3.Instance = TIM3; // Instance timer 3
        htim3.Init.Prescaler = 1; // Prescaler of 1
        htim3.Init.CounterMode = TIM_COUNTERMODE_UP; // Counter mode up
        htim3.Init.Period = 10000; // 10kHz signal
        htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
        
        sConfigPWM.OCMode = TIM_OCMODE_PWM1; // output compare mode PW1
        sConfigPWM.Pulse = 0; // Pulse = 0
        sConfigPWM.OCPolarity = TIM_OCPOLARITY_HIGH; // OC polarity high
        sConfigPWM.OCFastMode = TIM_OCFAST_DISABLE;


        // Initialise timer 3 in PWM mode //             
        HAL_TIM_PWM_Init(&htim3);

        // Configure timer 3 channel 1 in PWM mode //
        HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigPWM,TIM_CHANNEL_1);
        // 
        __HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_1,2500);
        // Start timer 3 channel 1 in PWM mode //
        HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_1);

        _is_init = 1;
    }  
 }


 void motor_encoder_init(void){

    // Enable GPIOC clock //
    __HAL_RCC_GPIOC_CLK_ENABLE(); 

    // Initialise PC0 and PC1 with 
    GPIO_InitTypeDef  GPIO_InitStructure; // Define structure to initialise GPIO
    GPIO_InitStructure.Pin = GPIO_PIN_0|GPIO_PIN_1; // Fill structure with correct parameters for pin, mode, pull and speed following lines
    GPIO_InitStructure.Mode = GPIO_MODE_IT_RISING_FALLING; 
    GPIO_InitStructure.Pull = GPIO_NOPULL; // No pull resistors enabled 
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;

    // Set IQR Priority //
    HAL_NVIC_SetPriority(EXTI0_IRQn, 0x0f, 0x0f);
    HAL_NVIC_SetPriority(EXTI1_IRQn, 0x0f, 0x0f);

    // Enable external interrupt for lines 0 and 1
    HAL_NVIC_EnableIRQ(EXTI0_IRQn); // Enable interrupt
    HAL_NVIC_EnableIRQ(EXTI1_IRQn); // Enable interrupt
    
 }

static uint8_t pin0;
static uint8_t pin1;
static uint32_t enc_count;

void EXTI0_IQRHandler(void){
    
    // Check if PC0 == PC1, adjust encoder count accordingly //
    pin0 = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0);
    pin1 = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1);

    if(pin0 == pin1){
        enc_count++;
    }   else {
            enc_count--;
    }

    // Reset the interrupt //
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}


void EXTI1_IQRHandler(void){
    
    // Check if PC0 == PC1, adjust encoder count accordingly //
    pin0 = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0);
    pin1 = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1);

    if(pin0 == pin1){
        enc_count--;
    }   else{
            enc_count++;
    }

    // Reset the interrupt //
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}



int32_t motor_encoder_getValue(void){

    return enc_count;

}