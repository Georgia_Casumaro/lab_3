#include "pendulum.h"
#include "stm32f4xx_hal.h"


// Function to initialise all of the hardware //

/*  Declare structures for ADC1
        TIP: Read the lab document and the HAL reference manual */
static ADC_HandleTypeDef hadc1;

ADC_ChannelConfTypeDef sConfigADC;

static uint8_t _is_init = 0;

void pendulum_init(void){

    if (!_is_init) {
        // Enable ADC1 Clock 
        __HAL_RCC_ADC1_CLK_ENABLE(); 

        // Enable GPIOB clock 
        __HAL_RCC_GPIOB_CLK_ENABLE(); 
          
        // Initialise PB0 //

        GPIO_InitTypeDef  GPIO_InitStructure; // Define structure to initialise GPIO
        GPIO_InitStructure.Pin = GPIO_PIN_0; // Fill structure with correct parameters for pin, mode, pull and speed following lines
        GPIO_InitStructure.Mode = GPIO_MODE_ANALOG; // External interrupts on the rising edge
        GPIO_InitStructure.Pull = GPIO_NOPULL; // Pulldown resistors enabled 
        GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;

        HAL_GPIO_Init(GPIOB, &GPIO_InitStructure); // Initialise GPIO using HAL function

        // Initialise ADC1

        hadc1.Instance = ADC1;
        hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
        hadc1.Init.Resolution = ADC_RESOLUTION_12B;
        hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
        hadc1.Init.ContinuousConvMode = DISABLE;
        hadc1.Init.NbrOfConversion = 1;

        // Configure ADC channel 
        sConfigADC.Channel = ADC_CHANNEL_8;
        sConfigADC.Rank = 1;
        sConfigADC.SamplingTime = ADC_SAMPLETIME_480CYCLES;
        sConfigADC.Offset = 0;
        
        HAL_ADC_Init(&hadc1); //Initialise the ADC         
        HAL_ADC_ConfigChannel(&hadc1, &sConfigADC); // Configure the ADC channel      


        _is_init = 1;
    }  
 }


float pendulum_read_voltage(void){

        float result = 0;

        // Start the ADC // 
        HAL_ADC_Start(&hadc1);

        // Poll the ADC for a conversion
        HAL_ADC_PollForConversion (&hadc1, 0xFF);    
        
        // Get and return the 12-bit result 
        result = HAL_ADC_GetValue(&hadc1); // Gets the converted value from data register of regular channel

        // Stop the ADC //
        HAL_ADC_Stop (&hadc1);

        // Compute the voltage from ADC reading //
        result = (result*3.3)/4095;

        return result; // Retuns the 12-bit result float already defined
}